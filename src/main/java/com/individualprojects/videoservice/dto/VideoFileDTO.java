package com.individualprojects.videoservice.dto;

import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"commentsDTO"})
@ToString(exclude = {"commentsDTO"})
public class VideoFileDTO {
    private Long id;
    private String videoCaption;
    private BigDecimal mediumMark;
    private String videoDescription;
    private List<UserCommentDTO> commentsDTO;
}
