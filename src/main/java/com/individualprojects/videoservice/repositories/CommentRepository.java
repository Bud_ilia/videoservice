package com.individualprojects.videoservice.repositories;

import com.individualprojects.videoservice.entities.UserComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<UserComment, Long>, QuerydslPredicateExecutor<UserComment> {
}
