package com.individualprojects.videoservice.services.impl;

import com.individualprojects.videoservice.dto.FileInfoDTO;
import com.individualprojects.videoservice.dto.UserCommentDTO;
import com.individualprojects.videoservice.dto.VideoFileDTO;
import com.individualprojects.videoservice.entities.UserComment;
import com.individualprojects.videoservice.entities.VideoFile;
import com.individualprojects.videoservice.mappers.VideoFileInfoMapper;
import com.individualprojects.videoservice.mappers.VideoFileMapper;
import com.individualprojects.videoservice.repositories.CommentRepository;
import com.individualprojects.videoservice.repositories.VideoRepository;
import com.individualprojects.videoservice.services.UserCommentService;
import com.individualprojects.videoservice.services.VideoFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class VideoFileServiceImpl implements VideoFileService {
    private final VideoRepository repository;
    private final VideoFileMapper mapper;
    private final CommentRepository commentRepository;
    private final UserCommentService commentService;
    private final VideoFileInfoMapper infoMapper;

    @Autowired
    public VideoFileServiceImpl(VideoRepository repository, VideoFileMapper mapper, CommentRepository commentRepository, UserCommentService commentService, VideoFileInfoMapper infoMapper) {
        this.repository = repository;
        this.mapper = mapper;
        this.commentRepository = commentRepository;
        this.commentService = commentService;
        this.infoMapper = infoMapper;
    }

    @Override
    public FileInfoDTO uploadFile(FileInfoDTO dto) {
        VideoFile saved = repository.save(VideoFile.builder()
                .fileWay(dto.getFileWay())
                .build());
        dto.setId(saved.getId());
        log.debug("Video file save successfully. Id = {}", dto.getId());
        return dto;
    }

    @Override
    public VideoFileDTO updateFileInfo(Long id, VideoFileDTO dto) {
        VideoFile video = repository.findById(id)
                .orElseThrow(() -> new RuntimeException("Video file not found by id = " + id));

        video.setVideoCaption(dto.getVideoCaption());
        video.setVideoDescription(dto.getVideoDescription());
        video.setMediumMark(dto.getMediumMark());
        repository.save(video);
        dto.setId(video.getId());

        for (UserCommentDTO comment : dto.getCommentsDTO())
            commentRepository.save(commentRepository.save(UserComment.builder()
                    .content(comment.getContent())
                    .videoFile(video)
                    .build()));
        List<UserComment> list = dto.getCommentsDTO().stream()
                .map(commentDto -> {
                    return commentRepository.save(UserComment.builder()
                            .videoFile(video)
                            .content(commentDto.getContent())
                            .build());
                }).collect(Collectors.toList());
        log.info("update file info by id {}", id);
        return dto;
    }

    @Override
    public VideoFileDTO getById(Long id) {
        return repository.findById(id)
                .map(mapper::toDto)
                .orElseThrow(() -> new RuntimeException("Video file not found"));
    }

    //TODO: Проблема! Не маппит комментарии. Коментарии в записи остаются пустыми.
    @Override
    public List<VideoFileDTO> getAllWithComments() {
        List<VideoFileDTO> list = repository.getAllWithComments().stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
        log.info("Number of records of video files = {}", list.size());
        return list;
    }

    @Override
    public boolean deleteById(Long id) {
        repository.findById(id)
                .orElseThrow(() -> new RuntimeException("Video file not found by id = {}" + id));
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            log.error("While delete video file error occurred. {}", e.getMessage());
            return false;
        }
        log.info("Video file deleted by id = {}", id);
        return true;
    }

    @Override
    public boolean deleteAllRecords() {
        try {
            repository.deleteAll();
        } catch (Exception e) {
            log.error("While deleting records in database error ocured. {}", e.getMessage());
            return false;
        }
        log.info("All records deleted");
        return true;
    }

    @Override
    public FileInfoDTO getInfoById(Long id) {
        return repository.findById(id)
                .map(infoMapper::toDto)
                .orElseThrow(() -> new RuntimeException("Video file not found"));
    }
}
