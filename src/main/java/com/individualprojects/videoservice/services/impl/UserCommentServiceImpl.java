package com.individualprojects.videoservice.services.impl;

import com.individualprojects.videoservice.dto.UserCommentDTO;
import com.individualprojects.videoservice.entities.UserComment;
import com.individualprojects.videoservice.mappers.UserCommentMapper;
import com.individualprojects.videoservice.repositories.CommentRepository;
import com.individualprojects.videoservice.services.UserCommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserCommentServiceImpl implements UserCommentService {
    private final CommentRepository repository;
    private final UserCommentMapper mapper;

    @Autowired
    public UserCommentServiceImpl(CommentRepository repository, UserCommentMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public UserCommentDTO update(Long id, UserCommentDTO dto) {
        UserComment comment = repository.findById(id)
                .map(entity -> mapper.update(dto, entity))
                .orElseThrow(() -> new RuntimeException("Comment not find by id = " + id));
        repository.save(comment);
        dto.setId(id);
        return dto;
    }

    @Override
    public boolean deleteById(Long id) {
        repository.findById(id).orElseThrow(() -> new RuntimeException("Comment not found by id = " + id));
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            log.error("While deleting comment by id = {}", id);
            return false;
        }
        log.info("Comment deleted by id = {}", id);
        return true;
    }

    @Override
    public boolean deleteAllComments() {
        try {
            repository.deleteAll();
        } catch (Exception e) {
            log.error("While deleting records in database error occured. {}", e.getMessage());
            return false;
        }
        log.info("All records deleted");
        return true;
    }

    @Override
    public List<UserCommentDTO> getAll() {
        List<UserCommentDTO> list = repository.findAll().stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
        log.info("Number of comments = {}", list.size());
        return list;
    }
}
