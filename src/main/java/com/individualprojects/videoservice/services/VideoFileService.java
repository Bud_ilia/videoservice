package com.individualprojects.videoservice.services;

import com.individualprojects.videoservice.dto.FileInfoDTO;
import com.individualprojects.videoservice.dto.VideoFileDTO;

import java.util.List;

public interface VideoFileService {

    FileInfoDTO uploadFile(FileInfoDTO dto);

    VideoFileDTO updateFileInfo(Long id, VideoFileDTO dto);

    VideoFileDTO getById(Long id);

    List<VideoFileDTO> getAllWithComments();

    boolean deleteById(Long id);

    boolean deleteAllRecords();

    FileInfoDTO getInfoById(Long id);
}
