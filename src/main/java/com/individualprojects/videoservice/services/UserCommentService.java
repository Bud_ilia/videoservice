package com.individualprojects.videoservice.services;

import com.individualprojects.videoservice.dto.UserCommentDTO;

import java.util.List;

public interface UserCommentService {

    UserCommentDTO update(Long id, UserCommentDTO dto);

    boolean deleteById(Long id);

    boolean deleteAllComments();

    List<UserCommentDTO> getAll();
}
