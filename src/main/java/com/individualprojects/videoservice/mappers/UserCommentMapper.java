package com.individualprojects.videoservice.mappers;

import com.individualprojects.videoservice.dto.UserCommentDTO;
import com.individualprojects.videoservice.entities.UserComment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface UserCommentMapper {

    UserComment toEntity(UserCommentDTO dto);

    UserCommentDTO toDto(UserComment entity);

    @Mapping(target = "id")
    UserComment update(UserCommentDTO dto, @MappingTarget() UserComment entity);

}
