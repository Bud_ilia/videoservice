package com.individualprojects.videoservice.mappers;

import com.individualprojects.videoservice.dto.VideoFileDTO;
import com.individualprojects.videoservice.entities.VideoFile;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, uses = {UserCommentMapper.class})
public interface VideoFileMapper {
    VideoFile toEntity(VideoFileDTO dto);

    VideoFileDTO toDto(VideoFile entity);

    @Mapping(target = "id")
    VideoFile update(VideoFileDTO dto, @MappingTarget() VideoFile entity);
}
