package com.individualprojects.videoservice.controllers;

import com.individualprojects.videoservice.dto.FileInfoDTO;
import com.individualprojects.videoservice.dto.VideoFileDTO;
import com.individualprojects.videoservice.services.VideoFileService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/video-file")
public class VideoFileController {

    private final VideoFileService service;

    @Autowired
    public VideoFileController(VideoFileService service) {
        this.service = service;
    }

    @PostMapping(value = "file", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<String> uploadFile(MultipartFile file) {
        String name = System.getProperty("user.home") + File.separator + file.getOriginalFilename();
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File(name)));
                stream.write(bytes);
                stream.close();
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Вам не удалось загрузить " + file.getOriginalFilename() + " => " + e.getMessage());
            }
        } else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Вам не удалось загрузить " + file.getOriginalFilename() + " потому что файл пустой.");
        service.uploadFile(FileInfoDTO.builder()
                .fileWay(file.getOriginalFilename())
                .build());
        return ResponseEntity.ok("Вы удачно загрузили " + file.getOriginalFilename() + " в " + file.getOriginalFilename() + "-uploaded !");
    }

    @PutMapping("/updated/{id}")
    @ResponseStatus(HttpStatus.OK)
    public VideoFileDTO updateVideoFileInfo(@PathVariable Long id, @RequestBody VideoFileDTO dto) {
        return service.updateFileInfo(id, dto);
    }

    @GetMapping("/get/{id}")
    @ResponseStatus(HttpStatus.OK)
    public VideoFileDTO getInfoByID(@PathVariable Long id) {
        return service.getById(id);
    }

    @GetMapping("/get/list")
    @ResponseStatus(HttpStatus.OK)
    public List<VideoFileDTO> getAll() {
        return service.getAllWithComments();
    }

    @DeleteMapping("/deleted/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        boolean res = service.deleteById(id);
        if (res) return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body("Запись по id = " + id + " удалена");
        else return ResponseEntity.status(HttpStatus.GONE)
                .body("Запись не удалена");
    }

    @DeleteMapping("/deleted/list")
    public ResponseEntity<String> deleteAllRecords() {
        boolean res = service.deleteAllRecords();
        if (res) return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body("Записи удалены");
        else return ResponseEntity.status(HttpStatus.GONE)
                .body("Записи не удалены");
    }

    @GetMapping("/downloaded/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void FileDownload(@PathVariable Long id, HttpServletResponse response) throws IOException {
        FileInfoDTO dto = service.getInfoById(id);
        byte[] reportBytes = null;
        String way = System.getProperty("user.home") + File.separator + dto.getFileWay();
        File result = new File(way);
        if (result.exists()) {
            InputStream inputStream = new FileInputStream(way);
            String type = URLConnection.guessContentTypeFromName(way);
            response.setHeader("Content-Disposition", "attachment; filename=" + way);
            response.setHeader("Content-Type", type);

            reportBytes = new byte[100];//New change
            OutputStream os = response.getOutputStream();//New change
            int read = 0;
            while ((read = inputStream.read(reportBytes)) != -1) {
                os.write(reportBytes, 0, read);
            }
            os.flush();
            os.close();
        }
    }


    @GetMapping("/preview/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<byte[]> FileWatchOnline(@PathVariable Long id, HttpServletResponse response) {
        ResponseEntity<byte[]> result = null;

        try {
            FileInfoDTO dto = service.getInfoById(id);
            String way = System.getProperty("user.home") + File.separator + dto.getFileWay();
            Path path = Paths.get(way);
            byte[] video = Files.readAllBytes(path);
            response.setStatus(HttpStatus.OK.value());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            headers.setContentLength(video.length);
            result = new ResponseEntity<byte[]>(video, headers, HttpStatus.OK);
        } catch (java.nio.file.NoSuchFileException e) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        } catch (Exception e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return result;
}
    @GetMapping("/preview1/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void  FileWatchOnline1(@PathVariable Long id, HttpServletResponse response)  {
        try {
            FileInfoDTO dto = service.getInfoById(id);
            String way = System.getProperty("user.home") + File.separator + dto.getFileWay();
            Path path = Paths.get(way);
            File file = new File(String.valueOf(path));
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            response.setHeader("Content-Disposition", "attachment; filename="+file.getName().replace(" ", "_"));
            InputStream iStream = new FileInputStream(file);
            IOUtils.copy(iStream, response.getOutputStream());
            response.flushBuffer();
        }catch (FileNotFoundException e) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        } catch (Exception e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
