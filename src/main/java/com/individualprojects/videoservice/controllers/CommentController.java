package com.individualprojects.videoservice.controllers;

import com.individualprojects.videoservice.dto.UserCommentDTO;
import com.individualprojects.videoservice.services.UserCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user-comments")
public class CommentController {
    private final UserCommentService service;

    @Autowired
    public CommentController(UserCommentService service) {
        this.service = service;
    }

    @PutMapping("/updated/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserCommentDTO update(@PathVariable Long id, @RequestBody UserCommentDTO dto) {
        return service.update(id, dto);
    }

    @GetMapping("/get/list")
    @ResponseStatus(HttpStatus.OK)
    public List<UserCommentDTO> getAll() {
        return service.getAll();
    }

    @DeleteMapping("/deleted/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        boolean res = service.deleteById(id);
        if (res) return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body("Запись по id = " + id + " удалена");
        else return ResponseEntity.status(HttpStatus.GONE)
                .body("Запись не удалена");
    }

    @DeleteMapping("/deleted/list")
    public ResponseEntity<String> deleteAllRecords() {
        boolean res = service.deleteAllComments();
        if (res) return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body("Записи удалены");
        else return ResponseEntity.status(HttpStatus.GONE)
                .body("Записи не удалены");
    }
}
