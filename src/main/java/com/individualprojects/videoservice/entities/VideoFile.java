package com.individualprojects.videoservice.entities;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"comments"})
@ToString(exclude = {"comments"})
public class VideoFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fileWay;

    private String videoCaption;

    @Column(scale = 2, precision = 15)
    private BigDecimal mediumMark;

    private String videoDescription;

    @OneToMany(mappedBy = "videoFile", cascade = {CascadeType.REMOVE})
    List<UserComment> comments;

}
