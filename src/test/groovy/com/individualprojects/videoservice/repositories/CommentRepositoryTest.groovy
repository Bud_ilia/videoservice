package com.individualprojects.videoservice.repositories

import com.individualprojects.videoservice.entities.UserComment
import com.individualprojects.videoservice.entities.VideoFile
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import spock.lang.Specification

import java.time.LocalDateTime

@DataJpaTest
class CommentRepositoryTest extends Specification {
    @Autowired
    CommentRepository repository

    def "save entity"() {
        given:
        def entity = repository.save(UserComment.builder()
                .content("entity-1")
                .build())

        expect:
        repository.findById(entity.getId()).isPresent()
    }

    def "delete entity"() {
        given:
        def entity = repository.save(UserComment.builder()
                .content("entity-1")
                .build())
        repository.deleteById(entity.getId())

        expect:
        !repository.findById(entity.getId()).isPresent()
    }

    def "update entity"() {
        given:
        def entity = repository.save(UserComment.builder()
                .content("entity-1")
                .build())
        entity.setContent("entity-2")
        repository.save(entity)
        expect:
        repository.findById(entity.getId()).get().getContent() == "entity-2"
    }

    def "get entities"() {
        given:
        repository.save(UserComment.builder()
                .content("entity-1")
                .build())
        repository.save(UserComment.builder()
                .content("entity-2")
                .build())
        repository.save(UserComment.builder()
                .content("entity-3")
                .build())
        expect:
        repository.findAll().size() == 3
    }
}
