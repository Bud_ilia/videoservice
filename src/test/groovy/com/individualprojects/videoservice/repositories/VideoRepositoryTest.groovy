package com.individualprojects.videoservice.repositories

import com.individualprojects.videoservice.entities.UserComment
import com.individualprojects.videoservice.entities.VideoFile
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import spock.lang.Specification

@DataJpaTest
class VideoRepositoryTest extends Specification {

    @Autowired
    VideoRepository repository

    @Autowired
    CommentRepository commentRepository

    def "Save entity"() {
        given:
        def entity1 = repository.save(VideoFile.builder()
                .fileWay('GGdsf')
                .videoCaption("afdsfs")
                .videoDescription("sdcdcd grsgrw")
                .build())

        def comment1 = commentRepository.save(UserComment.builder()
                .videoFile(entity1)
                .content("sdfdsf")
                .build())
        def comment2 = commentRepository.save(UserComment.builder()
                .videoFile(entity1)
                .content("sd fad ffd dsf")
                .build())

        expect:
        repository.findById(entity1.getId()).isPresent()
        commentRepository.findById(comment1.getId()).get().getVideoFile().getId() == entity1.getId()
        commentRepository.findById(comment2.getId()).get().getVideoFile().getId() == entity1.getId()
    }
}
