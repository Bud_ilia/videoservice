package com.individualprojects.videoservice.mappers

import com.individualprojects.videoservice.dto.FileInfoDTO
import com.individualprojects.videoservice.dto.UserCommentDTO
import com.individualprojects.videoservice.dto.VideoFileDTO
import com.individualprojects.videoservice.entities.UserComment
import com.individualprojects.videoservice.entities.VideoFile
import org.mapstruct.factory.Mappers
import spock.lang.Specification

class VideoFileMapperTest extends Specification {

    def "update"() {
        given:
        def mapper = Mappers.getMapper(VideoFileMapper)

        def entity = VideoFile.builder()
                .id(4)
                .videoCaption("fdggdfgdgf")
                .videoDescription("dfgdfgdg")
                .mediumMark(5 as BigDecimal)
                .build()

        def dto = VideoFileDTO.builder()
                .videoDescription("4353ytr")
                .mediumMark(7 as BigDecimal)
                .videoCaption("yydsfsd")
                .build()

        when:
        def result = mapper.update(dto, entity)

        then:
        result.videoCaption == "yydsfsd"
        result.videoDescription == "4353ytr"
        result.mediumMark == 7
        result.id == 4
    }

    def "ConvertToDto"() {
        given:
        def mapper = Mappers.getMapper(VideoFileMapper)

        def entity = VideoFile.builder()
                .id(4)
                .videoCaption("fdggdfgdgf")
                .videoDescription("dfgdfgdg")
                .mediumMark(5 as BigDecimal)
                .build()

        def comment = UserComment.builder()
                .content("sdfsfsf")
                .build()

//        List<UserComment> list = new ArrayList<>();
//        list.add(comment)
//        entity.setComments(list)

        when:
        def result = mapper.toDto(entity)

        then:
        result.videoCaption == "fdggdfgdgf"
        result.videoDescription == "dfgdfgdg"
        result.mediumMark == 5
//        result.commentsDTO.size() == 1
        result.id == 4
    }

    def "ConvertToEntity"() {
        given:
        def mapper = Mappers.getMapper(VideoFileMapper)

        def dto = VideoFileDTO.builder()
                .videoDescription("4353ytr")
                .mediumMark(7 as BigDecimal)
                .videoCaption("yydsfsd")
                .build()

        def comment = UserCommentDTO.builder()
                .content("sdfsfsf")
                .build()

//        List<UserCommentDTO> list = new ArrayList<>();
//        list.add(comment)
//        dto.setCommentsDTO(list)

        when:
        def result = mapper.toEntity(dto)

        then:
        result.videoCaption == "yydsfsd"
        result.videoDescription == "4353ytr"
        result.mediumMark == 7
//        result.comments.size() == 1
    }
}
