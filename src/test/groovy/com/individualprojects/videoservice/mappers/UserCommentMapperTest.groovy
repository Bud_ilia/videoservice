package com.individualprojects.videoservice.mappers

import com.individualprojects.videoservice.dto.UserCommentDTO
import com.individualprojects.videoservice.entities.UserComment
import org.mapstruct.factory.Mappers
import spock.lang.Specification

class UserCommentMapperTest extends Specification {

    def "update"() {
        given:
        def mapper = Mappers.getMapper(UserCommentMapper.class)

        def entity = UserComment.builder()
                .content("dfdsdssasd")
                .id(3)
                .build()

        def dto = UserCommentDTO.builder()
                .content("23")
                .build()

        when:
        def result = mapper.update(dto, entity)

        then:
        result.content == "23"
        result.id == 3
    }

    def "ConvertToDto"() {
        given:
        def mapper = Mappers.getMapper(UserCommentMapper.class)

        def entity = UserComment.builder()
                .content("dfdsdssasd")
                .id(3)
                .build()

        when:
        def result = mapper.toDto(entity)

        then:
        result.content == "dfdsdssasd"
        result.id == 3
    }

    def "ConvertToEntity"() {
        given:
        def mapper = Mappers.getMapper(UserCommentMapper.class)

        def dto = UserCommentDTO.builder()
                .content("23")
                .id(3)
                .build()
        when:
        def result = mapper.toEntity(dto)

        then:
        result.content == "23"
        result.id == 3
    }
}
