package com.individualprojects.videoservice.mappers

import com.individualprojects.videoservice.dto.FileInfoDTO
import com.individualprojects.videoservice.dto.UserCommentDTO
import com.individualprojects.videoservice.entities.UserComment
import com.individualprojects.videoservice.entities.VideoFile
import org.mapstruct.factory.Mappers
import spock.lang.Specification

class VideoFileInfoMapperTest extends Specification {

    def "update"() {
        given:
        def mapper = Mappers.getMapper(VideoFileInfoMapper)

        def entity = VideoFile.builder()
                .id(4)
                .fileWay("wsdv,lfdg")
                .build()

        def dto = FileInfoDTO.builder()
                .fileWay("dfgsdfgf")
                .build()

        when:
        def result = mapper.update(dto, entity)

        then:
        result.fileWay == "dfgsdfgf"
        result.id == 4
    }

    def "ConvertToDto"() {
        given:
        def mapper = Mappers.getMapper(VideoFileInfoMapper)

        def entity = VideoFile.builder()
                .id(4)
                .fileWay("wsdv,lfdg")
                .build()

        when:
        def result = mapper.toDto(entity)

        then:
        result.fileWay == "wsdv,lfdg"
        result.id == 4
    }

    def "ConvertToEntity"() {
        given:
        def mapper = Mappers.getMapper(VideoFileInfoMapper)

        def dto = FileInfoDTO.builder()
                .id(4)
                .fileWay("dfgsdfgf")
                .build()

        when:
        def result = mapper.toEntity(dto)

        then:
        result.fileWay == "dfgsdfgf"
        result.id == 4
    }

}
